﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PolkaDvij : MonoBehaviour
{
    public GameObject EndPoint;
    public GameObject NachPoint;
    public bool OK=false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void Dvij()
    {
        if (OK==true)
        {
            transform.position = Vector3.MoveTowards(transform.position, NachPoint.transform.position, 100);
            OK = false;
        }
        if (OK==false)
        {
            transform.position = Vector3.MoveTowards(transform.position, EndPoint.transform.position, 100);
            OK = true;
        }

    }
   
}
