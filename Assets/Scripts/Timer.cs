﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Timer : MonoBehaviour
{
    public float GameTime;
    Coroutine ThreedWstopWatch; // ссылка на адрес потока/экземпляр

    public void StartStopwatch()
    {
        ThreedWstopWatch = StartCoroutine(StopWatchThreed()); // создаешь поток и присваешь ссылку
    }

    void ResetStopWatch()
    {
        GameTime = 0;
    }

    void StopStopwatch()
    {
        StopCoroutine(ThreedWstopWatch);
    }


    IEnumerator StopWatchThreed() // мой собсвенный Update()
    {
        while (true) // начало Update (отрисовки кадра)
        {
            //GameTime += Time.deltaTime;
            GameTime -= 1;
            if (GameTime <= 0)
            {
                Application.LoadLevel(2);
            }
            yield return new WaitForSeconds(1); // В конце вернуть последний/текущий кадр
        }
    }
}

