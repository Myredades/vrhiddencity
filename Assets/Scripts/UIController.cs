﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UIController : MonoBehaviour
{
    
        public Timer timer;
        public TextMeshPro TimerText;

        private void Start()
        {
            timer.StartStopwatch();
        }

        void ShowTimer()
        {
            TimerText.text = timer.GameTime.ToString();
        }

        private void Update()
        {
            ShowTimer();
        }


    
}
